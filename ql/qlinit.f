      subroutine qlinit
      implicit none

      write(*,*) '===================================================='
      write(*,*) '  This is QCDLoop - version 1.97                    '
      write(*,*) '  Authors: Keith Ellis and Giulia Zanderighi        ' 
      write(*,*) '  (keith.ellis@durham.ac.uk, zanderi@mpp.mpg.de)    '
      write(*,*) '  For details see FERMILAB-PUB-07-633-T,OUTP-07/16P '
      write(*,*) '  arXiv:0712.1851 [hep-ph], published in            '
      write(*,*) '  JHEP 0802:002,2008.                               '
      write(*,*) '===================================================='
      
      call ffini
      call flush(6)

      end
